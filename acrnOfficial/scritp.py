"""
    ULTIMATE SCRIPT

    It uses data written in files:
        * follow_followers.txt
        * follow_following.txt
        * like_hashtags.txt
        * like_users.txt
    and do the job. This bot can be run 24/7.
"""

import os
import sys
import random
import schedule
import datetime
import threading
import time
import multiprocessing


sys.path.append(os.path.join(sys.path[0], '../'))
from instabot import Bot

counterLock = 0





# COLORAZIONI TESTO 
class bcolors:
    HEADER = '\033[95m' #VIOLETTO
    OKBLUE = '\033[94m' #BLU
    OKGREEN = '\033[92m' #VERDEACCESO
    WARNING = '\033[93m' 
    FAIL = '\033[91m' #ROSSO
    ENDC = '\033[0m' #FINECOLORAZIONE
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# INSERIMENTO VALORI MASSIMI SULLE AZIONI DA COMPIERE
print time.time();
bot = Bot()
bot.login()
print ""
mfg=  raw_input( bcolors.HEADER+ "Massimi follow giornalieri? " + bcolors.ENDC ) 
mug= raw_input("Massimi unfollow giornalieri? ")
mlg= raw_input("Massimi like giornalieri? ")
mcg= raw_input("Massimi commenti giornalieri? ")



# PRINT PREFERENZE VALORI MASSIMI PER AZIONE

print "                                   "
print " "
print bcolors.OKGREEN +"Ecco le tue preferenze per questo utente:  "
print " "
print "Hai inserito che vuoi:", mfg , " follow giornalieri"
print "Hai inserito che vuoi:", mug , " unfollow giornalieri"
print "Hai inserito che vuoi:", mlg , " like giornalieri"
print "Hai inserito che vuoi:", mcg , " commenti giornalieri"
print "                                   " + bcolors.ENDC

wipeFile= raw_input("Vuoi svuotare i file di configurazione? (s/n)")
if wipeFile=="s":
    open('follow_followers.txt', 'w').close()
    open('follow_following.txt', 'w').close()
    open('like_hashtags.txt', 'w').close()
    open('like_users.txt', 'w').close()
#QUA DEVO INSERIRE IL BACKUP#

# PRINT DELLE INFORMAZIONI SUL PIANO DI CRESCITA
print ""
print ""
print bcolors.OKGREEN +  "Programma di crescita per "+ bcolors.ENDC + bot.username 
follow_followers_list = bot.read_list_from_file("follow_followers.txt")
print  bcolors.HEADER +"Verranno seguiti i followers di: "+ bcolors.ENDC, follow_followers_list
follow_following_list = bot.read_list_from_file("follow_following.txt")
print bcolors.HEADER +"Verranno seguiti i seguiti da: " + bcolors.ENDC, follow_following_list
like_hashtags_list = bot.read_list_from_file("like_hashtags.txt")
print bcolors.HEADER +"Verra' applicato il like ai post con gli hashtag:"+ bcolors.ENDC, like_hashtags_list
like_users_list = bot.read_list_from_file("like_users.txt")
print bcolors.HEADER +"Verra' messo like ai seguenti utenti:" +bcolors.ENDC, like_users_list 
unfollow_following = bot.read_list_from_file("followed.txt")
print bcolors.HEADER +"Verranno Unfollowati i seguenti utenti ogni 2 giorni:" +bcolors.ENDC, unfollow_following 


#bloccker

def timeout(orario):
        
        timeout = datetime.datetime.now()
        if timeout.strftime("%H:%M")== orario:
                return True

#DEFINIZONE DELLE AZIONI DA SVOLGERE PER POI ANDARLE A STANZIARE ALL'INTERNO DELLO SCHEDULER

def followFollowers(massimo, orarioBreak):
    for item in follow_followers_list:
        print "Sto andando a seguire i followers di: ",item
        bot.follow_followers( item, vmfh) 
        if timeout(orarioBreak):
                return 
        print "Fine funzione"
    
    global counterLock
    counterLock = counterLock +1

def follow_following(massimo, orarioBreak):
    for item in follow_following_list:
        print item
        bot.follow_following (item, vmfh)
        
        if timeout(orarioBreak):
                return 
        print "Fine funzione"

    global counterLock
    counterLock = counterLock +1

def likeHashtagsList():
    for item in like_hashtags_list:
        print item
        bot.like_hashtag(item,) #{'hashtag': item, 'amount': None})

       

def likeUserList( massimo):
    for item in like_users_list:
        print "teoricamente sto andando a mettere like a: ",item
        bot.like_user(item , massimo)
        secondiLike= random.randint(2, 4)
        time.sleep(secondiLike)
    

def unfollowFollowing( massimo, orarioBreak):        
                        if timeout(orarioBreak):
                                return 
                        
                        bot.unfollow_users(unfollow_following)
                



     

#ALGORITMO RANDOM DI ASSEGNAZIONI DI AZIONI MASSIMI ALL' ORA

#max follow orari
def mfh(mfg):
        maxFollowOrari = int(mfg)/4
        minFollowOrari = maxFollowOrari- maxFollowOrari/3
        FollowOrari = random.randint(minFollowOrari,maxFollowOrari)
        return FollowOrari

#valore dei massimi follow orari
vmfh=  mfh(mfg)

#max like orari
def mlh(mlg):
        maxLikeOrari = int(mlg)/4
        minLikeOrari = maxLikeOrari- maxLikeOrari/3
        LikeOrari = random.randint(minLikeOrari,maxLikeOrari)
        return LikeOrari

vmlh = mlh (mlg)

#ASSEGNAZIONE DI THREAD
def run_threaded(job_fn):
    job_thread = threading.Thread(target=job_fn)
    job_thread.start()


#CREAZIONE TIMING RANDOM PER FOLLOW

minuti= random.randint(10,20)
followOrario10= "10:"+str(minuti)
followOrario16= "16:"+str(minuti)
followOrario20= "20:"+str(minuti)
followOrario12= "12:"+str(minuti)

#CREAZIONE TIMING RANDOM PER LIKE

minutiLike = random.randint(10,20)

#CREAZIONE DI FUNZIONI PER IL MULTIPROCESSING

def funLike():  
        schedule.every(minutiLike).minutes.do(run_threaded,likeUserList(vmlh))


def iniziaLoShow():
        print "show must go on"
        schedule.every().day.at(followOrario10).do(run_threaded,followFollowers(mfh, "11:45"))
        schedule.every().day.at(followOrario12).do(run_threaded,followFollowers(mfh, "15.37"))
        schedule.every().day.at(followOrario16).do(run_threaded,followFollowers(mfh, "19.02"))
        schedule.every().day.at(followOrario20).do(run_threaded,followFollowers(mfh, "22.27"))
                        

        


# INSERIMENTO INFORMAZIONI SULLE AZIONI DA COMPIERE
statoInserimento = "s"
inserimento = ""

print ""
print "Inserisci gli username degli utenti di cui vuoi seguire i followers"
file = open("follow_followers.txt","a")
while statoInserimento !="n":
        
        inserimento = raw_input(" Inserisci un nome utente ") 
        file.write(inserimento+'\n') 
        
        statoInserimento = raw_input("Vuoi aggiungere un nuovo utente? (s/n)")
file.close()

statoInserimento = "s"
inserimento = ""

print ""
print "Inserisci gli username degli utenti di cui vuoi seguire le persone che segue"
file = open("follow_following.txt","a")
while statoInserimento !="n":
        
        inserimento = raw_input(" Inserisci un nome utente ") 
        file.write(inserimento+'\n') 
        
        statoInserimento = raw_input("Vuoi aggiungere un nuovo utente? (s/n)")
file.close()


statoInserimento = "s"
inserimento = ""

print ""
print "Inserisci gli hashtag dei post a cui vuoi mettere like"
file = open("like_hashtag.txt","a")
while statoInserimento !="n":
        
        inserimento = raw_input(" Inserisci un hashtag (senza #)") 
        file.write(inserimento+'\n') 
        
        statoInserimento = raw_input("Vuoi aggiungere un nuovo hashtag? (s/n)")
file.close()
             

statoInserimento = "s"
inserimento = ""

print ""
print "Inserisci gli utenti a cui vuoi mettere like"
file = open("like_users.txt","a")
while statoInserimento !="n":
        
        inserimento = raw_input(" Inserisci un utente (senza @)") 
        file.write(inserimento+'\n') 
        
        statoInserimento = raw_input("Vuoi aggiungere un nuovo utente? (s/n)")
file.close()


#ALGORITMI DI CRESCITA A SELEZIONE 
algoritmoSelezione =0 
while algoritmoSelezione != 9:
        algoritmoSelezione=  raw_input( bcolors.HEADER+ "Quale algoritmo di crescita vuoi utilizzare ? \n 1. Follow + Like \n 2. Like (Coming Soon)\n 3. Follow (Coming Soon) \n 4. Commenti (Coming Soon)"  + bcolors.ENDC ) 
        if algoritmoSelezione == "1": 
                global counterLock
                print "Stato CounterLock: " , counterLock
                print "Hai scelto il primo algoritmo, puoi anche andare via, il software E' automatizzato 24/24"
                
                if counterLock < "8":
                        print followOrario10
                        print followOrario12
                        print followOrario16
                        print followOrario20

                        schedule.every().day.at("06:47").do(run_threaded,iniziaLoShow)
                        
                        while True:
                                schedule.run_pending()
                                time.sleep(1)
                        
                
                if counterLock == "8":
                        unfollowFollowing(mfg, "23:02") 
                        
                        
                        
                        while True:
                                schedule.run_pending()
                                time.sleep(1)
        
                        counterLock = 0

                        
                
        if algoritmoSelezione == "2":

                unfollowFollowing(mfg, "01:00") 
                        
                        
                        
                while True:
                        schedule.run_pending()
                        time.sleep(1)



